# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.


# QUESTIONS for guessing_game
# 1. how is this spec testing my output? The output looks correct
# but the spec reads more text
def guessing_game

  # random_nums = (1..100).to_a.shuffle
  # machine_pick = random_nums[0]
  machine_pick = rand(1..100)
  num_guesses = 0
  user_guess = 0


  until user_guess == machine_pick
    puts "guess a number"
    user_guess = gets.chomp.to_i
    num_guesses += 1

    if user_guess > machine_pick
      puts "#{user_guess}"
      puts "too high"
    elsif user_guess < machine_pick
      puts "#{user_guess}"
      puts "too low"
    end
  end

  puts "#{user_guess}"
  puts "#{num_guesses}"

end


#
# def file_shuffler
#
#   # program should prompt the user for a file name
#   # reads that file
#   # shuffles the lines
#   # saves it to the file "{input_name}-shuffled.txt"
#
#   print "What's the file name: "
#   file_name = gets.chomp
#   line_arr = []
#
#   user_file = File.open("#{file_name}.txt")
#   user_file.each { |line| line_arr << line }
#   line_arr.shuffle!
#
#   new_file = File.new("#{file_name}--shuffled.txt")
#
#   # check if this actually works
#   user_file { |line| new_file < line }
#
# end
